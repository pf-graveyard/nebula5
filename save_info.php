<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');

    $filename = $_POST['name'];
    $filedescription = $_POST['description'];
    $filepath = $_POST['path'];

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'edit.php', 'Go back');

    require 'config.php';

    $escaped_filename = mysql_real_escape_string($filename);
    $escaped_filedescription = mysql_real_escape_string($filedescription);
    $escaped_filepath = mysql_real_escape_string($filepath);

    $query = 'UPDATE '.$config['mysql_prefix'].'files SET name=\''.$escaped_filename.'\', description=\''.$escaped_filedescription.'\' WHERE path=\''.$escaped_filepath.'\';';
    $res = mysql_query($query, $db);
    if (!$res)
        show_error('db_error', 'edit.php', 'Go back');
    mysql_close($db);

    header('Location: main.php');
    die();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css" type="text/css">
        <title>Nebula5 — Error</title>
    </head>
    <body>
        <div class="top">
            <?php require 'top.php'; ?>
        </div>
        <div class="error">
            <?php echo $_POST['msg']; ?><br>
            <a href="<?php echo $_POST['link']; ?>"><?php echo $_POST['name']; ?></a>
        </div>
    </body>
</html>
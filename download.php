<?php
    require 'errors_wrapper.php';

    if (!isset($_GET['q']) || $_GET['q'] == '')
        show_error('no_file_to_download', 'main.php', 'Go back');

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'main.php', 'Go back');

    require 'config.php';

    $escaped_path = mysql_real_escape_string($_GET['q']);
    $query = 'SELECT name, size, path FROM '.$config['mysql_prefix'].'files WHERE path=\''.$escaped_path.'\';';
    $res = mysql_query($query, $db);
    if (!$res)
        show_error('download_error', 'main.php', 'Go back');
    $a_res = mysql_fetch_assoc($res);

    if (!isset($a_res['path']))
        show_error('download_error', 'main.php', 'Go back');

    $query = 'UPDATE '.$config['mysql_prefix'].'files SET downloads=downloads+1 WHERE path=\''.$escaped_path.'\';';
    $res = mysql_query($query, $db);
    if (!$res)
        show_error('download_error', 'main.php', 'Go back');
    
    mysql_close($db);

    $path = 'files/'. $escaped_path;

    header('Content-Type: application/octet-stream');
    header('Accept-Ranges: bytes');
    header('Content-Length: '.$a_res['size']);
    header('Content-Disposition: attachment; filename="'.$a_res['name'].'"');
    readfile($path);
?>

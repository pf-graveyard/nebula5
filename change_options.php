<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');

    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];

    if ($password1 == '' || $password2 == '')
    {
        header('Location: main.php');
        die();
    }

    if ($password1 != $password2)
        show_error('password_not_match', 'options.php', 'Go back');

    $hash = sha1($password1);

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'options.php', 'Go back');

    require 'config.php';

    $query = 'UPDATE '.$config['mysql_prefix'].'options SET sha1_hash=\''.$hash.'\';';
    $res = mysql_query($query, $db);
    if (!$res)
        show_error('options_change_error', 'options.php', 'Go back');
    mysql_close($db);

    header('Location: main.php');
    die();
?>
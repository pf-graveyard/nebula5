<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css" type="text/css">
        <title>Nebula5 — Upload</title>
    </head>
    <body>
        <div class="top">
            <?php require 'top.php'; ?>
        </div>
        <form method="post" action="process.php" enctype="multipart/form-data">
            <div class="uploadform">
                <table width="100%" border="0px">
                    <tr>
                        <td>File:</td><td class="left"><input name="file" type="file"></td>
                    </tr>
                    <tr>
                        <td>Description:</td><td class="left"><input name="description" type="text"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="right"><input type="submit" value="Upload"></td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
</html>
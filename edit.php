<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');

    $q = $_GET['q'];

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'main.php', 'Go back');

    require 'config.php';

    $escaped_q = mysql_real_escape_string($q);
    $query = 'SELECT name, description FROM '.$config['mysql_prefix'].'files WHERE path=\''.$escaped_q.'\';';
    $res = mysql_query($query, $db);
    if (!$res)
        show_error('db_error', 'main.php', 'Go back');
    $a_res = mysql_fetch_assoc($res);
    mysql_close($db);

    $filename = $a_res['name'];
    $filedescription = $a_res['description'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css" type="text/css">
        <title>Nebula5 — Edit</title>
    </head>
    <body>
        <div class="top">
            <?php require 'top.php'; ?>
        </div>
        <form method="post" action="save_info.php">
            <div class="editform">
                <table width="100%">
                    <tr>
                        <td>Name:</td><td><input name="name" type="text" value="<?php echo $filename; ?>"></td>
                    </tr>
                    <tr>
                        <td>Description:</td><td><input name="description" type="text" value="<?php echo $filedescription; ?>"></td>
                    </tr>
                    <tr>
			<td colspan="2" class="right"><input type="submit" value="Save"></td>
                    </tr>
                </table>
            </div>
            <input name="path" type="hidden" value="<?php echo $q; ?>">
        </form>
    </body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css" type="text/css">
        <title>Nebula5 — Login</title>
    </head>
    <body>
        <?php
            session_start();
            if (isset($_SESSION['loggedin']))
            {
                header('Location: main.php');
                die();
            }
        ?>
        <form method="post" action="login.php">
            <div class="loginform">
                <table width="100%" border="0px">
                    <tr>
                        <td>Password:</td><td class="right"><input name="password" type="password"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="right"><input type="submit" value="Log in"></td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
</html>

<?php
    session_start();
    require 'errors_wrapper.php';
    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');
    $_SESSION = array();
    session_destroy();
    header('Location: index.php');
    die();
?>

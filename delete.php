<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');

    $filepath = $_GET['q'];

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'main.php', 'Go back');

    require 'config.php';

    $escaped_filepath = mysql_real_escape_string($filepath);

    $query = 'DELETE FROM '.$config['mysql_prefix'].'files WHERE path=\''.$escaped_filepath.'\';';
    $res = mysql_query($query, $db);
    if (!$res)
        show_error('db_error', 'main.php', 'Go back');
    mysql_close($db);

    unlink('files/'.$escaped_filepath);

    header('Location: main.php');
    die();
?>
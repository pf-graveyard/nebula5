<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');

    $filename = $_FILES['file']['name'];
    $tmpname = $_FILES['file']['tmp_name'];
    $filesize = $_FILES['file']['size'];
    $filedescription = $_POST['description'];

    $filenamehash = sha1($filename.$tmpname.$filedescription.$filesize.time().rand(0, getrandmax()));
    $filepath = 'files/'.$filenamehash;

    $res = move_uploaded_file($tmpname, $filepath);
    if (!$res)
        show_error('file_upload_error', 'upload.php', 'Go back');

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'index.php', 'Go back');

    require 'config.php';

    $escaped_filename = mysql_real_escape_string($filename);
    $escaped_filedescription = mysql_real_escape_string($filedescription);

    $query = 'INSERT INTO '.$config['mysql_prefix'].'files (name, size, time, downloads, description, path) VALUES
        (\''.$escaped_filename.'\',
         '.$filesize.',
         FROM_UNIXTIME('.time().'),
         0,
         \''.$escaped_filedescription.'\',
         \''.$filenamehash.'\');';
    $res = mysql_query($query, $db);
    if (!$res)
    {
        unlink($filepath);
        show_error('file_upload_error', 'upload.php', 'Go back');
    }
    mysql_close($db);

    header('Location: main.php');
    die();
?>

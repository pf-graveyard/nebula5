SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS `nb5_files` (
  `name` varchar(256) NOT NULL,
  `size` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `downloads` int(11) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `path` varchar(41) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `nb5_options` (
  `sha1_hash` varchar(41) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `nb5_options` (`sha1_hash`) VALUES
('d033e22ae348aeb5660fc2140aec35850c4da997');
<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css" type="text/css">
        <title>Nebula5 — Files</title>
    </head>
    <body>
        <div class="top">
            <?php require 'top.php'; ?>
        </div>
        <br>
        <table width="100%">
            <tr class="header">
                <td>Name</td><td>Description</td><td>Downloads</td><td>Size, kbytes</td><td>Date&Time</td><td>Action</td>
            </tr>
            <tr><td colspan="6" class="empty">&nbsp;</td></tr>
            <?php
                require 'connect_db.php';
                $db = connect_db();
                if ($db == null)
                    show_error('db_connection_failed', 'index.php', 'Go back');

                require 'config.php';

                $query = 'SELECT name, downloads, description, size, time, path  FROM '.$config['mysql_prefix'].'files ORDER BY time DESC;';
                $res = mysql_query($query, $db);

                $index = 0;
                while ($a_res = mysql_fetch_assoc($res))
                {
                    $index++;
                    $row_style = '';
                    if ($index % 2 == 0)
                        $row_style = 'dark';
                    else
                        $row_style = 'light';
                    echo '<tr class="'.$row_style.'">
                        <td><a href="download.php?q='.$a_res['path'].'">'.$a_res['name'].'</a></td>
                        <td>'.$a_res['description'].'</td><td>'.$a_res['downloads'].'</td>
                        <td class="center">'.round($a_res['size'] / 1024).'</td>
                        <td class="center">'.date('d.m.Y H:i:s', strtotime($a_res['time'])).'</td>
                        <td class="center"><a href="edit.php?q='.$a_res['path'].'">[EDIT]</a> <a href="delete.php?q='.$a_res['path'].'">[DELETE]</a></td>
                        </tr>';
                }
                mysql_close($db);
            ?>
        </table>
    </body>
</html>
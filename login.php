<?php
    session_start();
    if (isset($_SESSION['loggedin']))
    {
        header('Location: main.php');
        die();
    }
    $password = sha1($_POST['password']);

    require 'errors_wrapper.php';

    require 'connect_db.php';
    $db = connect_db();
    if ($db == null)
        show_error('db_connection_failed', 'index.php', 'Go back');

    require 'config.php';

    $escaped_login = mysql_real_escape_string($login);
    $query = 'SELECT sha1_hash FROM '.$config['mysql_prefix'].'options;';
    $res = mysql_query($query, $db);
    $a_res = mysql_fetch_assoc($res);
    mysql_close($db);

    if ($a_res['sha1_hash'] == $password)
    {
        $_SESSION['loggedin'] = true;
        header('Location: main.php');
        die();
    } else
        show_error('incorrect_password', 'index.php', 'Go back');
?>

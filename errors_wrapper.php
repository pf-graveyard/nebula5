<?php
    function show_error($type, $link, $name)
    {
        $message = '';
        switch ($type)
        {
            case 'incorrect_password':
                $message = 'Incorrect password';
                break;
            case 'db_connection_failed':
                $message = 'Database connection failed';
                break;
            case 'not_logged_in':
                $message = 'You are not logged in';
                break;
            case 'file_upload_error':
                $message = 'File upload error';
                break;
            case 'no_file_to_download':
                $message = 'No file to download';
                break;
            case 'download_error':
                $message = 'File download error';
                break;
            case 'password_not_match':
                $message = 'Passwords do not match';
                break;
            case 'options_change_error':
                $message = 'Error saving options';
                break;
            case 'db_error':
                $message = 'Database error';
                break;
        }

        $encoded_message = urlencode($message);
        $encoded_link = urlencode($link);
        $encoded_name = urlencode($name);

        $post_data = 'msg='.$encoded_message.'&link='.$encoded_link.'&name='.$encoded_name;

        require 'config.php';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $config['url'].'/error.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_exec ($ch);
        curl_close ($ch);

        die();
    }
?>

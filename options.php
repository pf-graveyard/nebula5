<?php
    session_start();

    require 'errors_wrapper.php';

    if (!isset($_SESSION['loggedin']))
        show_error('not_logged_in', 'index.php', 'Go home');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css" type="text/css">
        <title>Nebula5 — Options</title>
    </head>
    <body>
        <div class="top">
            <?php require 'top.php'; ?>
        </div>
        <form method="post" action="change_options.php">
            <div class="optionsform">
                <table width="100%">
                    <tr><td>Password:</td><td><input name="password1" type="password"></td>
                    <tr><td>Repeat password:</td><td><input name="password2" type="password"></td>
                    <tr><td colspan="2" class="right"><input type="submit" value="Save"></td></tr>
                </table>
            </div>
        </form>
    </body>
</html>